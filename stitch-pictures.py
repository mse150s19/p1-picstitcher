import glob
from PIL import Image

image_file_list = glob.glob("team_images/*")
team_logo = Image.new('RGB', (128*len(image_file_list), 128)) #make a placeholder image

for i, image_file in enumerate(image_file_list):
    im = Image.open(image_file)
    im_resized = im.resize((128, 128))# Resize image to be 128 x 128
    team_logo.paste(im_resized, (128*i, 0))# Paste image into team logo 
team_logo.save("team_logo.png","PNG")# Save the team logo

